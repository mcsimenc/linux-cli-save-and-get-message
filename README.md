## A pair of scripts to save and access text between shell instances.

### Installation

```
git clone https://gitlab.com/mcsimenc/linux-cli-save-and-get-message.git
cd linux-cli-save-and-get-message
chmod +x save_var.sh
chmod +x get_var.sh
echo "alias s=`pwd`/save_var.sh" >> ~/.bashrc
echo "alias g=`pwd`/get_var.sh" >> ~/.bashrc
source ~/.bashrc
```

### Usage

```
$ s `pwd`
$ g
/home/msimenc/scripts/linux-cli-save-and-get-message
```

### Between shell instances example

```
$ screen
$ s `pwd`
$ Ctl-a + d
$ g
/home/msimenc/scripts/linux-cli-save-and-get-message
```
