#!/usr/bin/env bash
#
#   usage:
#       get_var.sh
#
#   description:
#       using save_var.sh <msg> is added to the file ~/VAR and can be retrieved
#       using get_var.sh
#
#   example:
#       screen
#       save_var.sh msg
#       Ctl-a + d
#       get_var.sh
#           msg


echo `cat ~/.VAR`
